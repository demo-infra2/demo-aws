import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import * as synced from "@pulumi/synced-folder";

//Creación de un host zone para alojar las alias de los load balancer, tener en cuenta que su costo radica en la creación de uno, a parte se tiene que comprar los derechos de uso del dominio para más información vea "https://www.notion.so/Alias-de-las-urls-016ac6b07abc4cbe8e5f6e96470faa1f"
//si usa variás veces pulumi up y pulumi down se estará creando y borrando constantemente la hostzone lo que generará grandes costos
/* const hostedZone = new aws.route53.Zone("hostedZone", {
    name: "demotestaws.com", // Reemplaza con tu dominio
});

//Crea un certificado para poder usar los alias
const certificate = new aws.acm.Certificate("certificate", {
    domainName: hostedZone.name,
    subjectAlternativeNames: ["*.demoTestAWS.com"],
    validationMethod: "EMAIL",
});
 */
/* //Usa una hosted zone ya existente
const hostedZone = {
    name: "",
    id: "",
}; */

//Usaando una función para la creación del bucket, controles, el acceso publico, los objetos y la distribución a Cloud Front//
function createS3Bucket(name: string): aws.s3.Bucket {
    //creación de los buckets//
    const bucket = new aws.s3.Bucket(name, {
        versioning: { enabled: true },
        website: { indexDocument: "main.js" },
    });

    //Creación de los controles//
    const controls = new aws.s3.BucketOwnershipControls(`${name}Controls`, {
        bucket: bucket.id,
        rule: { objectOwnership: "ObjectWriter" },
    });

    // Creación de el anulador del bloqueador del acceso publico//
    const bloqueo = new aws.s3.BucketPublicAccessBlock(`${name}Bloqueo`, {
        bucket: bucket.id,
        blockPublicAcls: false,
        blockPublicPolicy: false,
        ignorePublicAcls: false,
        restrictPublicBuckets: false,
    });

/*     //Sube las carpetas a los s3//
    const objetos = new synced.S3BucketFolder(`${name}Objetos`, {
        path: `./${path}`,
        bucketName: bucket.bucket,
        acl: aws.s3.PublicReadAcl,
    }, { dependsOn: [bloqueo, controls] }); */

    //Nos permite acceder a los servicios cloud front 
    const originAccessIdentity = new aws.cloudfront.OriginAccessIdentity(`${name}OriginAccessIdentity`);

    // Crea la distribución del S3 con Cloud front
    const distribution = new aws.cloudfront.Distribution(`${name}Distribution`, {
        
        //indica cual bucket s3 va a usar
        origins: [{
            domainName: bucket.bucketRegionalDomainName,
            originId: "s3Origin",
            s3OriginConfig: {
                originAccessIdentity: originAccessIdentity.cloudfrontAccessIdentityPath,
            },
        }],
        enabled: true,
        isIpv6Enabled: true,
        defaultRootObject: `main.js`,
        defaultCacheBehavior: {
            allowedMethods: ["GET", "HEAD"],
            cachedMethods: ["GET", "HEAD"],
            targetOriginId: "s3Origin",
            forwardedValues: {
                queryString: true,
                cookies: { forward: "none" },
            },
            viewerProtocolPolicy: "redirect-to-https",
            minTtl: 0,
            defaultTtl: 3600,
            maxTtl: 86400,
        },
        orderedCacheBehaviors: [{
            pathPattern: `/*`,
            allowedMethods: ["GET", "HEAD"],
            cachedMethods: ["GET", "HEAD"],
            targetOriginId: "s3Origin",
            forwardedValues: {
                queryString: false,
                cookies: { forward: "none" },
            },
            minTtl: 0,
            defaultTtl: 3600,
            maxTtl: 86400,
            viewerProtocolPolicy: "redirect-to-https",
        }],
        viewerCertificate: {
            cloudfrontDefaultCertificate: true,
        },
        restrictions: {
            geoRestriction: {
                restrictionType: "none",
            },
        },
    });

    //Crea un nuevo registro en donde se pondra un alias para la url del  cloud front 
/*     const registro = new aws.route53.Record(`${name}Record`, {
        name: `${name}.${hostedZone.name}`, // Usa el nombre como para la url como ejemplo api-demo-java.demoTestAWS.com
        type: "CNAME",
        ttl: 300, // Tiempo de vida en segundos
        records: [distribution.domainName], // Utiliza el nombre de dominio del CloudFront
        zoneId: hostedZone.id,
    },{ dependsOn: [distribution,hostedZone] }); */

    return bucket;
}

// Uso de la función para la creación de todo lo necesario, pon el nomobre del proyecto y el nombre de la carpeta//
// Para la creación de la url
//const apidemojavas3 = createS3Bucket("apidemojavaS3", "api-demo-java");
const demoangulars3 = createS3Bucket("demoangularS3");
/* const demoreacts3 = createS3Bucket("demoreactS3", "demo-react");
const demospas3 = createS3Bucket("demospaS3", "demo-spa");
const demovues3 = createS3Bucket("demovueS3", "demo-vue"); */

export const bucketNameAngular = demoangulars3.bucket;


